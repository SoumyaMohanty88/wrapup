/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var $ = require('gulp-load-plugins')();
var conf = require('./gulp/conf');
var runSequence = require('run-sequence');
var del = require('del');

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});


/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', function () {
  $.util.log('this is the default task');
});

gulp.task('clean:testResults', function (callback) {
  del(conf.paths.testResults, callback);
});

gulp.task('validate', ['clean:testResults'], function(){
    return runSequence('scripts:validate', 'markups:validate', 'index:validate', 'tests:unit');
});

gulp.task('ci:build', ['validate'], function () {
  gulp.start(['build:ci']);
});

// gulp.task('validate', ['scripts:validate', 'markups:validate', 'index:validate', 'tests:unit'], function () {
//   $.util.log('All is validated');
// }).on('error', function(){
//     $.util.log('Errors');
// });