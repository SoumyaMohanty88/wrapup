'use strict';

var webpackConfig = require('./webpack.config.js')('unitTests');

module.exports = function(config) {

  var configuration = {

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    files: [
      { pattern: 'client/test-context.js', watched: false }
    ],

    singleRun: true,

    autoWatch: false,

    frameworks: ['jasmine'],

    browsers : ['PhantomJS'],
    // browsers : ['Chrome', 'Firefox', 'Safari'],

    webpack: webpackConfig,

    webpackMiddleware: {
        // webpack-dev-middleware configuration
        noInfo: true // this just hides the webpack build output from the console
    },

    plugins : [
      'karma-phantomjs-launcher',
      'karma-firefox-launcher',
      'karma-chrome-launcher',
      'karma-safari-launcher',
      'karma-jasmine',
      'karma-webpack',
      'karma-coverage',
      'karma-threshold-reporter',
      'karma-junit-reporter'
    ],

    preprocessors: {
      'client/test-context.js': ['webpack'] //ensure that webpack and babel are used to transpile the files
    },

    reporters: ['progress','coverage', 'threshold', 'dots', 'junit'],

    coverageReporter: {
      // specify a common output directory
      dir: 'client/testResults',
      reporters: [
        // reporters not supporting the `file` property
        { type: 'lcov', subdir: 'report-lcov' },
        { type: 'cobertura', subdir: 'report-cobertura', file: 'coverage.xml' },
        { type: 'clover', subdir: 'report-clover', file: 'clover.xml'}
      ]
    },

    thresholdReporter: {
      statements: 93,
      branches: 93,
      functions: 93,
      lines: 93
    },

    junitReporter: {
      outputDir: 'client/testResults/junit', // results will be saved as $outputDir/$browserName.xml
      outputFile: 'test-results.xml', // if included, results will be saved as $outputDir/$browserName/$outputFile
      suite: '' // suite will become the package name attribute in xml testsuite element
    },

    browserNoActivityTimeout: 20000, // wait 20 seconds to receive messages from browser before aborting
  };


  config.set(configuration);
};