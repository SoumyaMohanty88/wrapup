'use strict';

var path = require('path'),
    extend = require('util')._extend,
    webpack = require('webpack'),
    ngAnnotatePlugin = require('ng-annotate-webpack-plugin');


var defaults = {

    context: path.resolve(__dirname, 'client/src'),
    entry: {
        app: './app/app.js',
        vendor: './app/vendor.js'
    },
    output: {
        path: path.resolve(__dirname, 'client/target'), // provided at execution time
        filename: './app/[name].js',
        chunkFilename: './app/[id].js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel',
            exclude: /(node_modules|bower_components)/,
            query: {
                // https://github.com/babel/babel-loader#options
                cacheDirectory: true,
                presets: ['es2015']
            }
        },
        {
            test: /\.html$/,
            loader: 'raw',
            exclude: /(node_modules|bower_components)/
        },
        {
            test: /\.css$/,
            loader: "css-loader"
        }]
    },
    resolve: {
        alias: {
          bower: path.join(__dirname, 'client/src/bower_components'),
          npm: path.join(__dirname, '/node_modules'),
          jquery: path.join(__dirname, 'client/src/bower_components/jquery/dist/jquery.js')
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin('vendor', './app/vendor.js'),
        new ngAnnotatePlugin({
            add: true
        })
    ]
};

var setConfig = function(configType) {

    var config;

    console.log('Webpack config type requested: ', configType);

    switch (configType) {
    
        case 'scriptsDev':

            config = extend(defaults, {
                devtool: 'eval',
                cache: true,
                watch: true,
                debug: true
            });

            break;

        case 'scriptsDist':

            config = extend({}, defaults);
            config.output.path = path.resolve(__dirname, 'client/dist');

            config.plugins.push(new webpack.optimize.UglifyJsPlugin({
                sourceMap: false,
                mangle: true
            }));

            break;

        case 'scriptsCi':

            config = extend(defaults, {
                devtool: 'eval'
            });

            break;


        case 'unitTests':

            // We only need certain aspects of the configuration here
            // leaving out entry/output, etc.
            config = {
              module: [],
              resolve: defaults.resolve,
              // *optional* babel options: isparta will use it as well as babel-loader
              babel: {
                  presets: ['es2015']
              },
              // *optional* isparta options: istanbul behind isparta will use it
              isparta: {
                  embedSource: true,
                  noAutoWrap: true,
                  // these babel options will be passed only to isparta and not to babel-loader
                  babel: {
                      presets: ['es2015']
                  }
              }
            };

            config.module.preLoaders = defaults.module.loaders;
            config.module.preLoaders.push({
                test: /\.js$/,
                exclude: [/node_modules/, /bower_components/, /\.spec\.js/],
                loader: 'isparta'
            });

            break;

    }

    return config;
};

module.exports = function(type) {
    
    var configType = type || 'scriptsDev';
    
    return setConfig(configType);

};